Task:
- Create 1 VPC with 2 Subnet (Public and Private)
- Connect NAT Gateway to private subnet
- Autoscalling group dengan 2 ec2 (t2.medium) yang akan scale-up 3instance ketika Treshold CPU >= 45% dan diletakan di dalam private subnet
- Create Dockerfile dengan simply file hello.txt ke root project yang berada di /var/www
- buat pipeline yml script yang menjalakan docker images hasil build dockerfile di atas, lalu install di targeted host yang kita telah regis di gitlab



Isi Directory:

1. terraform = berisi file hcl untuk kebutuh yang diinginkan sesuai soal di atas
2. Dockerfile = membuat images nginx dan copy file
3. gitlab-ci.yml = script pipeline
4. default = config nginx untuk set root project
5. hello.rxr = file yang akan di-copy
