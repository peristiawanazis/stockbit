variable "AWS_REGION" {
    default = "ap-southeast-1a"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}
variable "AMIS" {
  type = "map"
  default = {
    ap-shouteast-1 = "ami-xxxxxx"
  }
}
