resource "aws_launch_configuration" "test-launchconfig" {
  name_prefix          = "test-launchconfig"
  image_id             = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type        = "t2.medium"
  lifecycle              { create_before_destroy = true }
}

resource "aws_autoscaling_group" "test-autoscaling" {
  name                 = "test-autoscaling"
  vpc_zone_identifier  = "${aws_subnet.test-subnet-private-1.id}"
  launch_configuration = "${aws_launch_configuration.test-launchconfig.name}"
  min_size             = 2
  max_size             = 5
  desired_capacity     = 2
  health_check_grace_period = 300
  health_check_type = "ELB"
  load_balancers = ["${aws_elb.my-elb.name}"]
  force_delete = true

  tag {
      key = "Name"
      value = "ec2 instance"
      propagate_at_launch = true
  }
}
