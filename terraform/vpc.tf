resource "aws_vpc" "test_vpc" {
    cidr_block = "10.300.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    tags {
        Name = "test_vpc"
    }
}

#public subnet
resource "aws_subnet" "test-subnet-public-1" {
    vpc_id = "${aws_vpc.test_vpc.id}"
    cidr_block = "10.300.0.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "ap-southeast-1a"

    tags {
        Name = "test-subnet-public-1"
    }
}

#private subnet
resource "aws_subnet" "test-subnet-private-1" {
    vpc_id = "${aws_vpc.test_vpc.id}"
    cidr_block = "10.300.1.0/24"
    availability_zone = "ap-southeast-1a"

    tags {
        Name = "test-subnet-private-1"
    }
}

#elastic public ip
resource "aws_eip" "eip" {
  vpc=true
}


#nat gw
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.test-subnet-private-1.id}"
  tags = {
    Name = "nat_gw"
  }
}

#internet gateway
resource "aws_internet_gateway" "igw" {
 vpc_id = "${aws_vpc.test_vpc.id}"
 tags = {
    Name = "igw"
 }
}

# route tables
resource "aws_route_table" "rt-public" {
    vpc_id = "${aws_vpc.test_vpc.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.igw.id}"
    }

    tags {
        Name = "rt-public"
    }
}

# route associations public
resource "aws_route_table_association" "rta-public" {
    subnet_id = "${aws_subnet.test-subnet-public-1.id}"
    route_table_id = "${aws_route_table.rt-public.id}"
}
