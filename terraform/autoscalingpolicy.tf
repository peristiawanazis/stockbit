resource "aws_autoscaling_policy" "test-asg-policy" {
  name = "test-asg-policy"
  autoscaling_group_name = "${aws_autoscaling_group.test-autoscaling.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "1"
  cooldown = "300"
  policy_type = "SimpleScaling"
  }

$ scale-up
resource "aws_cloudwatch_metric_alarm" "test-cpu-alarm" {
  alarm_name = "test-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  estimated_instance_warmup = "60"
  threshold = "45"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.test-autoscaling.name}"
    }
  actions_enabled = true
  }

# back to normal position
resource "aws_autoscaling_policy" "test-cpu-alarm-down" {
  autoscaling_group_name = "${aws_autoscaling_group.test-autoscaling.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "-1"
  cooldown = "300"
  policy_type = "SimpleScaling"
  }

$ scale down
resource "aws_cloudwatch_metric_alarm" "test-cpu-alarm-scaledown" {
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "5"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.test-autoscaling.name}"
   }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.test-cpu-alarm-scaledown.arn}"]
}

